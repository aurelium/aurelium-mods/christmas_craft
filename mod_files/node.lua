-- ============== --
-- legacy support --
-- ============== --

minetest.register_alias("christmas_craft:snow_block", "default:snowblock")

minetest.register_alias("christmas_craft:silver_baubles", "christmas_craft:white_baubles")

minetest.register_alias("christmas_craft:Christmas_present", "christmas_craft:Christmas_present_white")

-- ========== --
-- misk nodes --
-- ========== --
minetest.register_node("christmas_craft:christmas_wreath", {
	description = "Christmas Wreath",
	drawtype = "signlike",
	walkable = false,
	tiles = {
		{name="christmas_wreath.png", animation={type="vertical_frames", aspect_w=32, aspect_h=32, length=3.0}},
		},
	inventory_image =  "christmas_wreath.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	selection_box = {
		type = "wallmounted",
	},
	groups = {oddly_breakable_by_hand = 3},
})

minetest.register_node("christmas_craft:christmas_star", {
	description = "christmas Star",
	drawtype = "plantlike",
	light_source = 10,
	tiles = {"christmas_star.png"},
	is_ground_content = true,
	groups = {crumbly=3},
	sounds = default.node_sound_sand_defaults(),
})

minetest.register_node("christmas_craft:christmas_leaves", {
	description = "Christmas leaves",
	drawtype = "allfaces_optional",
	tiles = {"christmas_leaves.png"},
	is_ground_content = false,
	paramtype = "light",
	groups = {crumbly=3},
	sounds = default.node_sound_sand_defaults(),
})


-- =========== --
--  Snow Block --
-- =========== --

-- snowman

minetest.register_node("christmas_craft:snowman", {
	description = "Snowman",
	tiles = {"default_snow.png", "default_snow.png", "default_snow.png",
		"default_snow.png", "default_snow.png", "Snowman_F.png"},
	is_ground_content = true,
	paramtype2 = "facedir",
	groups = {crumbly=3},
	sounds = default.node_sound_sand_defaults(),
})

-- snow steps --

minetest.register_node("christmas_craft:snow_slab",{
  description = "Snow Slab",
  drawtype = "nodebox",
  tiles = {"default_snow.png"},
  is_ground_content = true,
	walkable = false,
  paramtype = "light",
  paramtype2 = "facedir",
  groups = {crumbly=3},
	sounds = default.node_sound_dirt_defaults({
			footstep = {name = "default_snow_footstep", gain = 0.15},
			dug = {name = "default_snow_footstep", gain = 0.2},
			dig = {name = "default_snow_footstep", gain = 0.2}
		}),
	drop = 'default:snow',
  node_box = {
    type = "fixed",
    fixed = {
    {-0.5, -1, -0.5, 0.5, -0.625, 0.5}, -- NodeBox2
    }
  },
  selection_box = {
    type = "fixed",
    fixed = {
    {-0.5, -1, -0.5, 0.5, -0.4375, 0.5}, -- NodeBox2
    },
  },
})

minetest.register_node("christmas_craft:snow_steps",{
  description = "Snow Stairs",
  drawtype = "nodebox",
  tiles = {"default_snow.png"},
  is_ground_content = true,
	walkable = false,
  paramtype = "light",
  paramtype2 = "facedir",
  groups = {crumbly=3},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_snow_footstep", gain = 0.15},
		dug = {name = "default_snow_footstep", gain = 0.2},
		dig = {name = "default_snow_footstep", gain = 0.2}
	}),
	drop = 'default:snow 2',
  node_box = {
    type = "fixed",
    fixed = {
      {-0.5, -1, -0.5, 0.5, -0.625, 0}, -- NodeBox2
  			{-0.5, -0.5, 0, 0.5, -0.125, 0.5}, -- NodeBox3
    }
  },
})

minetest.register_node("christmas_craft:snow_steps_1",{
  description = "Snow Stairs",
  drawtype = "nodebox",
  tiles = {"default_snow.png"},
  is_ground_content = true,
	walkable = false,
  paramtype = "light",
  paramtype2 = "facedir",
  groups = {crumbly=3},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_snow_footstep", gain = 0.15},
		dug = {name = "default_snow_footstep", gain = 0.2},
		dig = {name = "default_snow_footstep", gain = 0.2}
	}),
	drop = 'default:snow 2',
  node_box = {
    type = "fixed",
    fixed = {
      {0, -1, -0.5, 0.5, -0.625, 0}, -- NodeBox2
  			{-0.5, -0.5, 0, 0.5, -0.125, 0.5}, -- NodeBox3
  			{-0.5, -0.5, -0.5, 0, -0.125, 0.0625}, -- NodeBox4
    }
  },
})

minetest.register_node("christmas_craft:snow_steps_2",{
  description = "Snow Stairs",
  drawtype = "nodebox",
  tiles = {"default_snow.png"},
  is_ground_content = true,
	walkable = false,
  paramtype = "light",
  paramtype2 = "facedir",
  groups = {crumbly=3},
	sounds = default.node_sound_dirt_defaults({
			footstep = {name = "default_snow_footstep", gain = 0.15},
			dug = {name = "default_snow_footstep", gain = 0.2},
			dig = {name = "default_snow_footstep", gain = 0.2}
		}),
	drop = 'default:snow 2',
  node_box = {
    type = "fixed",
    fixed = {
      {-0.5, -1, -0.5, 0.5, -0.625, 0}, -- NodeBox2
  			{-0.5, -0.5, 0, 0, -0.125, 0.5}, -- NodeBox3
  			{0, -1, 0, 0.5, -0.625, 0.5}, -- NodeBox5
    }
  },
})

minetest.register_node("christmas_craft:christmas_pudding", {
	description = "Christmas Pudding",
	tiles = {
		"christmas_pud-top.png",
		"christmas_pud-bot.png",
		"christmas_pud-side.png",
	},
	drawtype = "nodebox",
	paramtype = "light",
	groups = {crumbly=3},
	sounds = default.node_sound_sand_defaults(),
		on_use = minetest.item_eat(8),
	node_box = {
		type = "fixed",
		fixed = {
			{-0.3125, -0.5, -0.3125, 0.3125, 0.0625, 0.3125}, -- NodeBox2
			{-0.0625, 0.0625, 0, 0, 0.125, 0.0625}, -- NodeBox4
			{0, 0.0625, -0.0625, 0.0625, 0.125, 0}, -- NodeBox5
		}
	}
})

local stocking_formspec = [[
size[8,9]
list[context;main;0,0.3;8,4;]
list[current_player;main;0,4.85;8,1;]
list[current_player;main;0,6.08;8,3;8]
listring[context;main]
listring[current_player;main]
]]

minetest.register_node("christmas_craft:stocking", {
	description = "Christmas Stocking",
	drawtype = "signlike",
	walkable = false,
	tiles =
	{name="christmas_stocking.png", animation={type="vertical_frames", aspect_w=32, aspect_h=32, length=3.0}},
	inventory_image =  "christmas_stocking.png",
	wield_image = "christmas_stocking.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	selection_box = {
	type = "wallmounted",
	},
	groups = {oddly_breakable_by_hand = 3},
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", stocking_formspec )
		meta:set_string("infotext", "Christmas Stocking")
		local inv = meta:get_inventory()
		inv:set_size("main", 16)
	end,
	can_dig = function(pos,player)
		local meta = minetest.get_meta(pos);
		local inv = meta:get_inventory()
		return inv:is_empty("main")
	end,
	on_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, count, player)
		minetest.log("action", player:get_player_name()..
				" moves stuff in box at "..minetest.pos_to_string(pos))
	end,
	on_metadata_inventory_put = function(pos, listname, index, stack, player)
		minetest.log("action", player:get_player_name()..
				" moves stuff to box at "..minetest.pos_to_string(pos))
	end,
	on_metadata_inventory_take = function(pos, listname, index, stack, player)
		minetest.log("action", player:get_player_name()..
				" takes stuff from box at "..minetest.pos_to_string(pos))
	end,
})
-- lights --

minetest.register_node("christmas_craft:christmas_wall_lights", {
	description = "christmas Wall lights",
	drawtype = "signlike",
	light_source = 10,
	walkable = false,
	tiles = {
		{name="christmas_lights_animated.png", animation={type="vertical_frames", aspect_w=32, aspect_h=32, length=3.0}},
		},
	inventory_image =  "christmas_lights.png",
	wield_image = "christmas_lights.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	selection_box = {
		type = "wallmounted",
	},
	groups = {oddly_breakable_by_hand = 3},
})

minetest.register_node("christmas_craft:christmas_lights", {
	description = "Christmas Lights",
	tiles = {
		{name="christmas_lights_animated.png", animation={type="vertical_frames", aspect_w=32, aspect_h=32, length=3.0}},
		},
	inventory_image =  "christmas_lights.png",
	wield_image = "christmas_lights.png",
	drawtype = "nodebox",
	walkable = false,
	light_source = 10,
	paramtype = "light",
  paramtype2 = "facedir",
  groups = {crumbly=3},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, 0.25, 0, 0.5, 0.5, 0}, -- NodeBox7
		}
	},
	selection_box = {
    type = "fixed",
    fixed = {
    {-0.5, 0.25, -0.125, 0.5, 0.5, 0.125}, -- NodeBox13
    },
  },
})

-- candy cain --
minetest.register_node("christmas_craft:candy_cane", {
	description = "Candy Cane",
	drawtype = "torchlike",
	--tiles = {"default_torch_on_floor.png", "default_torch_on_ceiling.png", "default_torch.png"},
	tiles = {"christmas_candy_cain_stick_wall.png"},
	inventory_image = "christmas_candy_cain_stick.png",
	wield_image = "christmas_candy_cain_stick.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	on_use = minetest.item_eat(1),
	selection_box = {
		type = "wallmounted",
		wall_side = {-0.5, -0.3, -0.1, -0.5+0.3, 0.3, 0.1},
	},
	groups = {choppy=2,dig_immediate=3,flammable=1,stick=1},
	legacy_wallmounted = true,
	sounds = default.node_sound_defaults(),
})

minetest.register_node("christmas_craft:candy_cane_node", {
	description = "Giant Candy Cane",
	tiles = {"christmas-candy_cabe.png",},
	is_ground_content = true,
	paramtype2 = "facedir",
	groups = { choppy = 2, oddly_breakable_by_hand = 1, flammable = 2},
	sounds = default.node_sound_wood_defaults(),
})

minetest.register_node("christmas_craft:candy_cane_tree", {
	description = "Candy Cane Tree",
	tiles = {"christmas-candy_cabe_top.png", "christmas-candy_cabe_top.png",
		"christmas-candy_cabe.png"},
	paramtype2 = "facedir",
	is_ground_content = false,
	groups = { choppy = 2, oddly_breakable_by_hand = 1, flammable = 2},
	sounds = default.node_sound_wood_defaults(),

	on_place = minetest.rotate_node
})

minetest.register_node(":default:snow", {
	description = "Snow",
	tiles = {"default_snow.png"},
	inventory_image = "default_snowball.png",
	wield_image = "default_snowball.png",
	paramtype = "light",
	buildable_to = true,
	walkable = false,
	floodable = true,
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.125, 0.5},
		},
	},
	groups = {crumbly = 3, falling_node = 1, puts_out_fire = 1},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name = "default_snow_footstep", gain = 0.15},
		dug = {name = "default_snow_footstep", gain = 0.2},
		dig = {name = "default_snow_footstep", gain = 0.2}
	}),

	on_construct = function(pos)
		pos.y = pos.y - 1
		if minetest.get_node(pos).name == "default:dirt_with_grass" then
			minetest.set_node(pos, {name = "default:dirt_with_snow"})
		end
	end,
})
