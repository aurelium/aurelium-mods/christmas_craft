
dofile(minetest.get_modpath("christmas_craft").."/mod_files/multi-node.lua")

dofile(minetest.get_modpath("christmas_craft").."/mod_files/node.lua")

dofile(minetest.get_modpath("christmas_craft").."/mod_files/items.lua")


--load configuration file from world folder
local MODPATH = minetest.get_modpath("christmas_craft")
local worldpath = minetest.get_worldpath()
local config = Settings(worldpath.."/christmas_craft.conf")

local conf_table = config:to_table()




--look into readme.md how to change settings
local defaults = {
enable_snowing = "true",
enable_crafts = "true",
}

--if not in conf file, create it.
for k, v in pairs(defaults) do
if conf_table[k] == nil then
config:set(k, v)
config:write();
end
end

-- if mtfood is installed
if minetest.get_modpath("mtfoods") ~=nil then
	minetest.log("info", "found mtfoods mod. execute mtfoods.lua")
	dofile(MODPATH .."/mod_support/mtfoods.lua")--if the 4seasons mod is installed execute this file
end


--if snow enabled, let it snow
if config:get("enable_snowing") == "true"  then
	minetest.log("info","let it snow, let it snow, let it snow.")
	dofile(MODPATH .."/snow.lua")--if snow enabled, execute this file
end

--if crafts enabled, register the craft recieps
if config:get("enable_crafts") == "true"  then
	minetest.log("info","registering craft rezieps for snow mod")
	dofile(MODPATH .."/mod_files/crafts.lua")--if snow enabled, execute this file
end

--overwrite the default stick

minetest.register_node(":default:stick", {
	description = "stick",
	drawtype = "torchlike",
	--tiles = {"default_torch_on_floor.png", "default_torch_on_ceiling.png", "default_torch.png"},
	tiles = {"side_stick.png"},
	inventory_image = "side_stick.png",
	wield_image = "side_stick.png",
	paramtype = "light",
	paramtype2 = "wallmounted",
	sunlight_propagates = true,
	walkable = false,
	selection_box = {
		type = "wallmounted",
		wall_side = {-0.5, -0.3, -0.1, -0.5+0.3, 0.3, 0.1},
	},
	groups = {choppy=2,dig_immediate=3,flammable=1,stick=1},
	legacy_wallmounted = true,
	sounds = default.node_sound_defaults(),
})
-- complex node  --
--[[
snowball_DAMAGE=0.5
snowball_GRAVITY=9
snowball_VELOCITY=19

--Shoot snowball.
local snow_shoot_snowball=function (item, player, pointed_thing)
	local playerpos=player:getpos()
	local obj=minetest.env:add_entity({x=playerpos.x,y=playerpos.y+1.5,z=playerpos.z}, "christmas_craft:snowball_entity")
	local dir=player:get_look_dir()
	obj:setvelocity({x=dir.x*snowball_VELOCITY, y=dir.y*snowball_VELOCITY, z=dir.z*snowball_VELOCITY})
	obj:setacceleration({x=dir.x*-3, y=-snowball_GRAVITY, z=dir.z*-3})
	item:take_item()
	return item
end


--The snowball Entity
snow_snowball_ENTITY={
	physical = false,
	timer=0,
	damage=1,
	gravity=10,
	velocity=19,
	range=1,
	textures = {"snowball.png"},
	lastpos={},
	collisionbox = {-0.25,-0.25,-0.25, 0.25,0.25,0.25},

}


--Snowball_entity.on_step()--> called when snowball is moving.

snow_snowball_ENTITY.on_step = function(self, dtime)
	self.timer=self.timer+dtime
	local pos = self.object:getpos()
	local node = minetest.env:get_node(pos)

	--Become item when hitting a node.
	if self.lastpos.x~=nil then --If there is no lastpos for some reason.
		if node.name ~= "air" then
			self.object:remove()
		end
		if node.name == "default:water_source" then
			minetest.sound_play("snowball_splash",
			{pos = pos, gain = 1.0, max_hear_distance = 32,})
			self.object:remove()
		end

	end
	self.lastpos={x=pos.x, y=pos.y, z=pos.z} -- Set lastpos-->Node will be added at last pos outside the node

end

minetest.register_entity("christmas_craft:snowball_entity", snow_snowball_ENTITY)

--Snowball.
minetest.register_craftitem("christmas_craft:snowball", {
	Description = "Snowball",
	inventory_image = "snowball.png",
	on_use = snow_shoot_snowball,
})

--Snow.
minetest.register_node("christmas_craft:snow", {
	tiles = {"snow.png"},
	drawtype = "nodebox",
	sunlight_propagates = true,
	paramtype = "light",
	param2 = nil,
	--param2 is reserved for what vegetation is hiding inside.
	--mapgen defines the vegetation.
	--1 = Moss
	groups = {crumbly=3,melts=1,falling_node=1},
	buildable_to = true,
	drop = 'christmas_craft:snowball',
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.35, 0.5}
		},
	},
	selection_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.5, 0.5, -0.35, 0.5}
		},
	},
	sounds = default.node_sound_dirt_defaults({
		footstep = {name="default_gravel_footstep", gain=0.45},
	}),
})
]]--
