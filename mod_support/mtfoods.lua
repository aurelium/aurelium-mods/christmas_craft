
minetest.register_craft({
	output = "christmas_craft:christmas_pudding_mix",
	recipe = {
		{"mtfoods:sugar","mtfoods:sugar","mtfoods:sugar"},
		{"default:apple","farming:flour","default:apple"},
		{"farming:flour","default:apple","farming:flour"},
	}
})

-- candy cain

minetest.register_craft({
	output = "christmas_craft:candy_cane",
	recipe = {
    {"","mtfoods:sugar","mtfoods:sugar"},
		{"","mtfoods:sugar",""},
		{"mtfoods:sugar","",""},
	}
})

minetest.register_craft({
	output = "christmas_craft:ginger_mix",
	recipe = {
		{"mtfoods:sugar","mtfoods:sugar"},
		{"farming:flour","farming:flour"},
	}
})
